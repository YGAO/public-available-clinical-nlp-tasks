# Publicly Available Clinical NLP Tasks 

An up-to-date review of all publicly available Clinical Natural Language Processing tasks, i.e. clinical NLP tasks that use publicly available EHR data from a cohort of patients.  

## Introduction 
This gitlab repository provides a full list of publicly available Clinical NLP tasks from our JAMIA paper: "**A Scoping Review of Publicly Available Language Tasks in Clinical Natural Language Processing**", Yanjun Gao, Dmitriy Dligach, Leslie Christensen, Samuel Tesch, Ryan Laffin, Dongfang Xu, Timothy Miller, Ozlem Uzuner, Matthew M Churpek, Majid Afshar.[Link](https://academic.oup.com/jamia/advance-article-abstract/doi/10.1093/jamia/ocac127/6654732?redirectedFrom=PDF).  We aim to summarize the progress of the field of cNLP as a resource to the community. We will keep track of the most recent tasks and update this repository when a new task (with public dataset) is released. 

Please cite this paper if you are using this repository: 

```
@article{10.1093/jamia/ocac127,
    author = {Gao, Yanjun and Dligach, Dmitriy and Christensen, Leslie and Tesch, Samuel and Laffin, Ryan and Xu, Dongfang and Miller, Timothy and Uzuner, Ozlem and Churpek, Matthew M and Afshar, Majid},
    title = "{A scoping review of publicly available language tasks in clinical natural language processing}",
    journal = {Journal of the American Medical Informatics Association},
    year = {2022},
    month = {08},
    issn = {1527-974X},
    doi = {10.1093/jamia/ocac127},
    url = {https://doi.org/10.1093/jamia/ocac127},
    note = {ocac127},
    eprint = {https://academic.oup.com/jamia/advance-article-pdf/doi/10.1093/jamia/ocac127/45227456/ocac127.pdf},
```


## HowtoUse

We provide two excel sheets with full lists of paper reviews. 
<!-- ```
ScopingReview-Tasks: with publication year, task types, descriptions, data source, link to download, publication venue, number of participants (if it is a shared task), impact on clinical decision support, reference. 

ScopingReview-Data:  with data split size, inter-annotator agreement, evaluation metric, baseline system and performance (if available), best system and performance (if it is a shared task). 

``` --> 
- [ScopingReview-Tasks]
We provide the description of tasks in an excel sheet. The structure of the table is formated as below: 

| Year | NLP Task Type | Challenge/Task/Dataset Name | Paper Bib Key | Task Description | Challenge/Shared Task? | Data Type | Data Use Agreement Required | Data Source | Currently Available | #Participants (systems or teams) if a shared task | Venue (Journal/Conference) | Community | Comments | Impact on Clinical Decision Support | Reference |
|------|---------------|-----------------------------|---------------|------------------|------------------------|-----------|-----------------------------|-------------|---------------------|---------------------------------------------------|----------------------------|-----------|----------|-------------------------------------|-----------|

- [ScopingReview-Data] 
We provide the description of dataset used in the existing papers. The structure of the table is formated as below: 

| Year | Challenge/Task/Dataset Name | Data Size (Default: # notes) Train \| Dev \| Test | Inter-Annotator Agreement? (If Y, Cohen's kappa unless other metrics specified) | Evaluation Metric | Best Performance (Within the shared task/challenge ?) | Best System | Baseline Best Performance | Baseline Best System | Paper Reference (for quick indexing) |
|------|-----------------------------|---------------------------------------------------|---------------------------------------------------------------------------------|-------------------|-------------------------------------------------------|-------------|---------------------------|----------------------|--------------------------------------|


- [ScopingReview-DatNotAvailable]
We provide a list of papers found in our literature search where the dataset is not publicly available.


Below, we also provide the lists of papers categorized by NLP task types (under Quick View-NLP Tasks), and by the type of clinical decision support applications (under Quick View-Clinical Decision Support).  


## Table of Contents
- [Quick View-NLP Tasks](#nlp)
  * [Coreference resolution](#coref)
  * [Document Classification](#doclass)
  * [Entity Linking](#el)
  * [Information Extraction](#ie)
  * [Lexical Normalization](#ln)
  * [Name Entity Recognition (NER)](#ner)
  * [Natural Language Inference (NLI)](#nli)
  * [Syntactic Parsing](#parsing)
  * [Question Answering](#qa)
  * [Semantic Textual Similarity](#sts)
  * [Sentence Classification](#sentclass)
  * [Speech Recognition](#speech)
  * [Summarization](#summ)
  * [Text Generation](#gen)
- [Quick View-Clinical Decision Support](#clinical)
  * [Action Item Extraction](#action)
  * [Cohort Selection for Clinical Trials](#cohort)
  * [Disorder Detection](#disorder)
  * [Obesity Classification](#obesity)
  * [Phenotyping](#phenotyping)
  * [Protected Health Information (PHI) De-Identification](#phi)
  * [Risk Factor Identification](#risk)
  * [Smoking Status Classification](#smoking)
  * [Substance Abuse Detection](#substance)
  * [Symptom Severity Prediction](#symptoms)
  * [General Non-Specific](#general) 
- [Authors](#author)
- [Acknowledgement](#ack)
- [Contact](#contact)

<!-- toc -->
<a name="nlp"> </a>
## [Quick View-NLP Tasks](https://git.doit.wisc.edu/YGAO/public-available-clinical-nlp-tasks/#nlp)



<a name="coref"> </a> 
### Coreference Resolution 
| Name                                             | Paper                                                                                                                                                                     |
|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Fifth i2b2/VA workshop on coreference resolution | "Evaluating the state of the art in coreference resolution for electronic medical records." Journal of the American Medical Informatics Association 19.5 (2012): 786-791. |

<a name="doclass"> </a>
### Document Classification 

| Name                                    | Paper                                                                                                                                                                                                                                        |
|-----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| i2b2 shared task on obesity challenge   | Uzuner, Özlem. "Recognizing obesity and comorbidities in sparse data." Journal of the American Medical Informatics Association 16.4 (2009): 561-570.                                                                                         |
| i2b2   shared task on smoking challenge | Uzuner, Özlem, et al. "Identifying patient smoking status from medical discharge records." Journal of the American Medical Informatics Association 15.1 (2008): 14-24.                                                                       |
| 2016 CEGS N-GRID shared tasks Track 2   | Filannino, Michele, Amber Stubbs, and Özlem Uzuner. "Symptom severity prediction from neuropsychiatric clinical records: Overview of 2016 CEGS N-GRID Shared Tasks Track 2." Journal of biomedical informatics 75 (2017): S62-S70.           |
| n2c2 2018 shared task track 1           | Stubbs, Amber, et al. "Cohort selection for clinical trials: n2c2 2018 shared task track 1." Journal of the American Medical Informatics Association 26.11 (2019): 1163-1171.                                                                |
| ACTdb                                   | Moseley, Edward T., et al. "A Corpus for Detecting High-Context Medical Conditions in Intensive Care Patient Notes Focusing on Frequently Readmitted Patients." Proceedings of the 12th Language Resources and Evaluation Conference. 2020.  |
| Social Determinants of Health           | Lybarger, Kevin, Mari Ostendorf, and Meliha Yetisgen. "Annotating social determinants of health using active learning, and characterizing determinants using neural event extraction." Journal of Biomedical Informatics 113 (2021): 103631. |

<!--- Uzuner, Özlem. "Recognizing obesity and comorbidities in sparse data." Journal of the American Medical Informatics Association 16.4 (2009): 561-570.

Uzuner, Özlem, et al. "Identifying patient smoking status from medical discharge records." Journal of the American Medical Informatics Association 15.1 (2008): 14-24.

Filannino, Michele, Amber Stubbs, and Özlem Uzuner. "Symptom severity prediction from neuropsychiatric clinical records: Overview of 2016 CEGS N-GRID Shared Tasks Track 2." Journal of biomedical informatics 75 (2017): S62-S70.

Stubbs, Amber, et al. "Cohort selection for clinical trials: n2c2 2018 shared task track 1." Journal of the American Medical Informatics Association 26.11 (2019): 1163-1171.

Moseley, Edward T., et al. "A Corpus for Detecting High-Context Medical Conditions in Intensive Care Patient Notes Focusing on Frequently Readmitted Patients." Proceedings of the 12th Language Resources and Evaluation Conference. 2020.

Lybarger, Kevin, Mari Ostendorf, and Meliha Yetisgen. "Annotating social determinants of health using active learning, and characterizing determinants using neural event extraction." Journal of Biomedical Informatics 113 (2021): 103631. ---> 


<a name="el"> </a>
### Entity Linking 
<!--- Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013.

Pradhan, Sameer, et al. "Semeval-2014 task 7: Analysis of clinical text." Proc. of the 8th International Workshop on Semantic Evaluation (SemEval 2014. 

Henry, Sam, et al. "The 2019 National Natural language processing (NLP) Clinical Challenges (n2c2)/Open Health NLP (OHNLP) shared task on clinical concept normalization for clinical records." Journal of the American Medical Informatics Association 27.10 (2020): 1529-1537. ---> 

| Name                                           | Paper                                                                                                                                                                                                         |
|------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2013 CLEF eHealth IR Task                      | Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013. |
| ShARe/CLEF eHealth 2013 Evaluation Lab Task 1b | Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013. |
| SemEval Task 7:Analysis of Clinical Text       | Pradhan, Sameer, et al. "Semeval-2014 task 7: Analysis of clinical text." Proc. of the 8th International Workshop on Semantic Evaluation. SemEval 2014.                                                       |


<a name="ie"> </a>
### Information Extraction 

<!--Uzuner, Özlem, Imre Solti, and Eithon Cadag. "Extracting medication information from clinical text." Journal of the American Medical Informatics Association 17.5 (2010): 514-518.

Uzuner, Özlem, et al. "2010 i2b2/VA challenge on concepts, assertions, and relations in clinical text." Journal of the American Medical Informatics Association 18.5 (2011): 552-556.

Uzuner, Ozlem, et al. "Evaluating the state of the art in coreference resolution for electronic medical records." Journal of the American Medical Informatics Association 19.5 (2012): 786-791.

Sun, Weiyi, Anna Rumshisky, and Ozlem Uzuner. "Evaluating temporal relations in clinical text: 2012 i2b2 Challenge." Journal of the American Medical Informatics Association 20.5 (2013): 806-813.

Kelly, Liadh, et al. "Overview of the share/clef ehealth evaluation lab 2014." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Cham, 2014.

Bethard, S., G. Savova, and M. Palmer. "SemEval-2017 Task 12." Clinical TempEval (2017).

Henry, Sam, et al. "2018 n2c2 shared task on adverse drug events and medication extraction in electronic health records." Journal of the American Medical Informatics Association 27.1 (2020): 3-12.

Viani, Natalia, et al. "Annotating Temporal Relations to Determine the Onset of Psychosis Symptoms." MedInfo. 2019.

Jagannatha, Abhyuday, et al. "Overview of the first natural language processing challenge for extracting medication, indication, and adverse drug events from electronic health record notes (MADE 1.0)." Drug safety 42.1 (2019): 99-111.--> 

| Name                                                                    | Paper                                                                                                                                                                                                                                      |
|-------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Third i2b2 Workshop on extracting medication information                | Uzuner, Özlem, Imre Solti, and Eithon Cadag. "Extracting medication information from clinical text." Journal of the American Medical Informatics Association 17.5 (2010): 514-518.                                                         |
| 2010 i2b2/VA Challenge                                                  | Uzuner, Özlem, et al. "2010 i2b2/VA challenge on concepts, assertions, and relations in clinical text." Journal of the American Medical Informatics Association 18.5 (2011): 552-556.                                                      |
| Fifth i2b2/VA workshop on coreference resolution (mention extraction)   | Uzuner, Ozlem, et al. "Evaluating the state of the art in coreference resolution for electronic medical records." Journal of the American Medical Informatics Association 19.5 (2012): 786-791.                                            |
| 2012 i2b2 Temporal Relations Challenges                                 | Sun, Weiyi, Anna Rumshisky, and Ozlem Uzuner. "Evaluating temporal relations in clinical text: 2012 i2b2 Challenge." Journal of the American Medical Informatics Association 20.5 (2013): 806-813.                                         |
| 2014 CLEF eHealth Task 2 (Disorder mention extraction)                  | Kelly, Liadh, et al. "Overview of the share/clef ehealth evaluation lab 2014." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Cham, 2014.                                               |
| 2017 SemEval Task 12                                                    | Bethard, S., G. Savova, and M. Palmer. "SemEval-2017 Task 12." Clinical TempEval (2017).                                                                                                                                                   |
| 2018 n2c2 shared task on adverse drug events and medication extraction  | Henry, Sam, et al. "2018 n2c2 shared task on adverse drug events and medication extraction in electronic health records." Journal of the American Medical Informatics Association 27.1 (2020): 3-12.                                       |
| Temporal Relations to Determine the Onset of Psychosis Symptoms         | Viani, Natalia, et al. "Annotating Temporal Relations to Determine the Onset of Psychosis Symptoms." MedInfo. 2019.                                                                                                                        |
| 2018 MADE 1.0 Task 2                                                    | Jagannatha, Abhyuday, et al. "Overview of the first natural language processing challenge for extracting medication, indication, and adverse drug events from electronic health record notes (MADE 1.0)." Drug safety 42.1 (2019): 99-111. |
| Assertion Detection    | van Aken, Betty, et al. "Assertion Detection in Clinical Notes: Medical Language Models to the Rescue?." Proceedings of the Second Workshop on Natural Language Processing for Medical Conversations. 2021.                               |

<a name="ln"> </a>
### Text Disambiguation and Normalization

<!-- Mowery, Danielle L., et al. "Normalizing acronyms and abbreviations to aid patient understanding of clinical texts: ShARe/CLEF eHealth Challenge 2013, Task 2." Journal of biomedical semantics 7.1 (2016): 1-13. ---> 

| Name                                     | Paper                                                                                                                                                                                                             |
|------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ShARe/CLEF eHealth Challenge 2013 Task 2 | Mowery, Danielle L., et al. "Normalizing acronyms and abbreviations to aid patient understanding of clinical texts: ShARe/CLEF eHealth Challenge 2013, Task 2." Journal of biomedical semantics 7.1 (2016): 1-13. |



<a name="ner"> </a>
### Name Entity Recognition (NER)

| Name                                                                       | Paper                                                                                                                                                                                                                                                                                                                                                |
|----------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2006 i2b2 Protected Health Information (PHI) De-Identification             | Uzuner, Özlem, Yuan Luo, and Peter Szolovits. "Evaluating the state-of-the-art in automatic de-identification." Journal of the American Medical Informatics Association 14.5 (2007): 550-563.                                                                                                                                                        |
| 2012 i2b2 Temporal Relation Challenges                                     | Sun, Weiyi, Anna Rumshisky, and Ozlem Uzuner. "Evaluating temporal relations in clinical text: 2012 i2b2 Challenge." Journal of the American Medical Informatics Association 20.5 (2013): 806-813.                                                                                                                                                   |
| 2013 CLEF eHealth Disorder NER (also in 2016 Mowery et. al)                | Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013.                                                                                                                                        |
| i2b2 UTHealth Corpus / i2b2 UTHealth De-identification shared task Track 1 | Stubbs, Amber, et al. "Identifying risk factors for heart disease over time: Overview of 2014 i2b2/UTHealth shared task Track 2." Journal of biomedical informatics 58 (2015): S67-S77.                                                                                                                                                              |
| i2b2 UTHealth Risk Factor Track (Track 2)                                  | Stubbs, Amber, et al. "Identifying risk factors for heart disease over time: Overview of 2014 i2b2/UTHealth shared task Track 2." Journal of biomedical informatics 58 (2015): S67-S77.                                                                                                                                                              |
| UW HarborView Phenotyping Event Extraction                                 | Klassen, Prescott, Fei Xia, and Meliha Yetisgen-Yildiz. "Annotating and detecting medical events in clinical notes." Proceedings of the Tenth International Conference on Language Resources and Evaluation (LREC'16). 2016.                                                                                                                         |
| Harvey Corpus                                                              | Savkov, Aleksandar, et al. "Annotating patient clinical records with syntactic chunks and named entities: the Harvey Corpus." Language resources and evaluation 50.3 (2016): 523-548.                                                                                                                                                                |
| Substance abuse from social history                                        | Yetisgen, Meliha, and Lucy Vanderwende. "Automatic identification of substance abuse from social history in clinical text." Conference on Artificial Intelligence in Medicine in Europe. Springer, Cham, 2017.                                                                                                                                       |
| CEGS N-GRID Shared task track 1                                            | Stubbs, Amber, Michele Filannino, and Özlem Uzuner. "De-identification of psychiatric intake records: Overview of 2016 CEGS N-GRID Shared Tasks Track 1." Journal of biomedical informatics 75 (2017): S4-S18.                                                                                                                                       |
| MADE 1.0 Task 1                                                            | Jagannatha, Abhyuday, et al. "Overview of the first natural language processing challenge for extracting medication, indication, and adverse drug events from electronic health record notes (MADE 1.0)." Drug safety 42.1 (2019): 99-111.                                                                                                           |
| 2019 N2C2/OHNLP                                                            | Shen, Feichen, et al. "Family history extraction from synthetic clinical narratives using natural language processing: overview and evaluation of a challenge data set and solutions for the 2019 National NLP Clinical Challenges (n2c2)/Open Health Natural Language Processing (OHNLP) competition." JMIR Medical Informatics 9.1 (2021): e24008. |

<!---
Uzuner, Özlem, Yuan Luo, and Peter Szolovits. "Evaluating the state-of-the-art in automatic de-identification." Journal of the American Medical Informatics Association 14.5 (2007): 550-563.

Pradhan, Sameer, et al. "Evaluating the state of the art in disorder recognition and normalization of the clinical narrative." Journal of the American Medical Informatics Association 22.1 (2015): 143-154.

Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013.

Sun, Weiyi, Anna Rumshisky, and Ozlem Uzuner. "Evaluating temporal relations in clinical text: 2012 i2b2 Challenge." Journal of the American Medical Informatics Association 20.5 (2013): 806-813.

Stubbs, Amber, and Özlem Uzuner. "Annotating longitudinal clinical narratives for de-identification: The 2014 i2b2/UTHealth corpus." Journal of biomedical informatics 58 (2015): S20-S29.

Stubbs, Amber, et al. "Identifying risk factors for heart disease over time: Overview of 2014 i2b2/UTHealth shared task Track 2." Journal of biomedical informatics 58 (2015): S67-S77.

Klassen, Prescott, Fei Xia, and Meliha Yetisgen-Yildiz. "Annotating and detecting medical events in clinical notes." Proceedings of the Tenth International Conference on Language Resources and Evaluation (LREC'16). 2016.

Savkov, Aleksandar, et al. "Annotating patient clinical records with syntactic chunks and named entities: the Harvey Corpus." Language resources and evaluation 50.3 (2016): 523-548.

Yetisgen, Meliha, and Lucy Vanderwende. "Automatic identification of substance abuse from social history in clinical text." Conference on Artificial Intelligence in Medicine in Europe. Springer, Cham, 2017.

Stubbs, Amber, Michele Filannino, and Özlem Uzuner. "De-identification of psychiatric intake records: Overview of 2016 CEGS N-GRID Shared Tasks Track 1." Journal of biomedical informatics 75 (2017): S4-S18.

Jagannatha, Abhyuday, et al. "Overview of the first natural language processing challenge for extracting medication, indication, and adverse drug events from electronic health record notes (MADE 1.0)." Drug safety 42.1 (2019): 99-111.

Shen, Feichen, et al. "Family history extraction from synthetic clinical narratives using natural language processing: overview and evaluation of a challenge data set and solutions for the 2019 National NLP Clinical Challenges (n2c2)/Open Health Natural Language Processing (OHNLP) competition." JMIR Medical Informatics 9.1 (2021): e24008.

---> 

<a name="nli"> </a>
### Natural Language Inference (NLI)
<!-- Romanov, Alexey, and Chaitanya Shivade. "Lessons from Natural Language Inference in the Clinical Domain." Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing. 2018.

Abacha, Asma Ben, Chaitanya Shivade, and Dina Demner-Fushman. "Overview of the mediqa 2019 shared task on textual inference, question entailment and question answering." Proceedings of the 18th BioNLP Workshop and Shared Task. 2019.
--> 
| Name                 | Paper                                                                                                                                                                                                                                    |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  MedNLI              | Romanov, Alexey, and Chaitanya Shivade. "Lessons from Natural Language Inference in the Clinical Domain." Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing. 2018.                                  |
| 2019 MEDIQA Task 1&2 | Abacha, Asma Ben, Chaitanya Shivade, and Dina Demner-Fushman. "Overview of the mediqa 2019 shared task on textual inference, question entailment and question answering." Proceedings of the 18th BioNLP Workshop and Shared Task. 2019. |


<a name="parsing"> </a>
### Syntactic Parsing 
| Name                                       | Paper                                                                                                                                                                                                                        |
|--------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| UW HarborView Phenotyping Event Extraction | Klassen, Prescott, Fei Xia, and Meliha Yetisgen-Yildiz. "Annotating and detecting medical events in clinical notes." Proceedings of the Tenth International Conference on Language Resources and Evaluation (LREC'16). 2016. |
| Harvey Corpus                              | Savkov, Aleksandar, et al. "Annotating patient clinical records with syntactic chunks and named entities: the Harvey Corpus." Language resources and evaluation 50.3 (2016): 523-548.                                        |

<!--- Klassen, Prescott, Fei Xia, and Meliha Yetisgen-Yildiz. "Annotating and detecting medical events in clinical notes." Proceedings of the Tenth International Conference on Language Resources and Evaluation (LREC'16). 2016.

Savkov, Aleksandar, et al. "Annotating patient clinical records with syntactic chunks and named entities: the Harvey Corpus." Language resources and evaluation 50.3 (2016): 523-548. ---> 



<a name="qa"> </a>
### Question Answering 
<!--- Yue, Xiang, Bernal Jiménez Gutiérrez, and Huan Sun. "Clinical Reading Comprehension: A Thorough Analysis of the emrQA Dataset." Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics. 2020.

Abacha, Asma Ben, Chaitanya Shivade, and Dina Demner-Fushman. "Overview of the mediqa 2019 shared task on textual inference, question entailment and question answering." Proceedings of the 18th BioNLP Workshop and Shared Task. 2019. --> 
| Name                           | Paper                                                                                                                                                                                                                                    |
|--------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CliniRC Task and emrQA Dataset | Yue, Xiang, Bernal Jiménez Gutiérrez, and Huan Sun. "Clinical Reading Comprehension: A Thorough Analysis of the emrQA Dataset." Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics. 2020.           |
| 2019 MEDIQA Task 3             | Abacha, Asma Ben, Chaitanya Shivade, and Dina Demner-Fushman. "Overview of the mediqa 2019 shared task on textual inference, question entailment and question answering." Proceedings of the 18th BioNLP Workshop and Shared Task. 2019. |


<a name="sts"> </a>
### Semantic Textual Similarity 
<!-- Wang, Yanshan, et al. "The 2019 n2c2/OHNLP track on clinical semantic textual similarity: overview." JMIR Medical Informatics 8.11 (2020): e23375. --> 
| Name            | Paper                                                                                                                                              |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| 2019 n2c2/OHNLP | Wang, Yanshan, et al. "The 2019 n2c2/OHNLP track on clinical semantic textual similarity: overview." JMIR Medical Informatics 8.11 (2020): e23375. |


<a name="sentclass"> </a>
### Sentence Classification 
| Name                   | Paper                                                                                                                                                                                                                                     |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| NegBio                 | Peng, Yifan, et al. "NegBio: a high-performance tool for negation and uncertainty detection in radiology reports." AMIA Summits on Translational Science Proceedings 2018 (2018): 188.                                                    |
| CLIP                   | Mullenbach, J., Pruksachatkun, Y., Adler, S., Seale, J., Swartz, J., McKelvey, T. G., Yang, Y., & Sontag, D. (2021). CLIP: A Dataset for Extracting Action Items for Physicians from Hospital Discharge Notes (version 1.0.0). PhysioNet. |

<!--| 2010 i2b2/VA Challenge | Uzuner, Özlem, et al. "2010 i2b2/VA challenge on concepts, assertions, and relations in clinical text." Journal of the American Medical Informatics Association 18.5 (2011): 552-556.                                                     |-->
<!--| Assertion Detection    | van Aken, Betty, et al. "Assertion Detection in Clinical Notes: Medical Language Models to the Rescue?." Proceedings of the Second Workshop on Natural Language Processing for Medical Conversations. 2021.                               |--> 

<!-- Uzuner, Özlem, et al. "2010 i2b2/VA challenge on concepts, assertions, and relations in clinical text." Journal of the American Medical Informatics Association 18.5 (2011): 552-556.

Peng, Yifan, et al. "NegBio: a high-performance tool for negation and uncertainty detection in radiology reports." AMIA Summits on Translational Science Proceedings 2018 (2018): 188.

van Aken, Betty, et al. "Assertion Detection in Clinical Notes: Medical Language Models to the Rescue?." Proceedings of the Second Workshop on Natural Language Processing for Medical Conversations. 2021.

Mullenbach, J., Pruksachatkun, Y., Adler, S., Seale, J., Swartz, J., McKelvey, T. G., Yang, Y., & Sontag, D. (2021). CLIP: A Dataset for Extracting Action Items for Physicians from Hospital Discharge Notes (version 1.0.0). PhysioNet. --> 


<a name="speech"> </a>
### Speech Recognition
<!--- Goeuriot, Lorraine et al. “Overview of the CLEF eHealth Evaluation Lab 2015.” CLEF (2015). ---> 
| Name                                    | Paper                                                                                      |
|-----------------------------------------|--------------------------------------------------------------------------------------------|
| CLEFeHealth evaluation lab 2015 Task 1a | Goeuriot, Lorraine et al. “Overview of the CLEF eHealth Evaluation Lab 2015.” CLEF (2015). |


<a name="summ"> </a>
### Summarization 
<!-- Abacha, Asma Ben, et al. "Overview of the mediqa 2021 shared task on summarization in the medical domain." Proceedings of the 20th Workshop on Biomedical Language Processing. 2021. --> 
| Name                                    | Paper                                                                                      |
|-----------------------------------------|--------------------------------------------------------------------------------------------|
| MEDIQA 2021 | Abacha, Asma Ben, et al. "Overview of the mediqa 2021 shared task on summarization in the medical domain." Proceedings of the 20th Workshop on Biomedical Language Processing. 2021. |


<a name="gen"> </a>
### Text Generation 
<!-- Wang, Ping, Tian Shi, and Chandan K. Reddy. "Text-to-sql generation for question answering on electronic medical records." Proceedings of The Web Conference 2020. 2020. --> 

| Name                                    | Paper                                                                                      |
|-----------------------------------------|--------------------------------------------------------------------------------------------|
| Text2SQL |  Wang, Ping, Tian Shi, and Chandan K. Reddy. "Text-to-sql generation for question answering on electronic medical records." Proceedings of The Web Conference 2020.  |

<a name="clinical"> </a>
## Quick View-Clinical Decision Support

<a name="action"> </a>
### Action Item Extraction 
Mullenbach, J., Pruksachatkun, Y., Adler, S., Seale, J., Swartz, J., McKelvey, T. G., Yang, Y., & Sontag, D. (2021). CLIP: A Dataset for Extracting Action Items for Physicians from Hospital Discharge Notes (version 1.0.0). PhysioNet.

<a name="cohort"> </a>
### Cohort Selection for Clinical Trials
Stubbs, Amber, et al. "Cohort selection for clinical trials: n2c2 2018 shared task track 1." Journal of the American Medical Informatics Association 26.11 (2019): 1163-1171.

<a name="disorder"> </a>
### Disorder Detection 
Kelly, Liadh, et al. "Overview of the share/clef ehealth evaluation lab 2014." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Cham, 2014.

Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013.

<a name="obesity"> </a>
### Obesity Classification
Uzuner, Özlem. "Recognizing obesity and comorbidities in sparse data." Journal of the American Medical Informatics Association 16.4 (2009): 561-570.

<a name="phenotyping"> </a>
### Phenotyping
Klassen, Prescott, Fei Xia, and Meliha Yetisgen-Yildiz. "Annotating and detecting medical events in clinical notes." Proceedings of the Tenth International Conference on Language Resources and Evaluation (LREC'16). 2016.

Moseley, Edward T., et al. "A Corpus for Detecting High-Context Medical Conditions in Intensive Care Patient Notes Focusing on Frequently Readmitted Patients." Proceedings of the 12th Language Resources and Evaluation Conference. 2020.

Lybarger, Kevin, Mari Ostendorf, and Meliha Yetisgen. "Annotating social determinants of health using active learning, and characterizing determinants using neural event extraction." Journal of Biomedical Informatics 113 (2021): 103631.

Shen, Feichen, et al. "Family history extraction from synthetic clinical narratives using natural language processing: overview and evaluation of a challenge data set and solutions for the 2019 National NLP Clinical Challenges (n2c2)/Open Health Natural Language Processing (OHNLP) competition." JMIR Medical Informatics 9.1 (2021): e24008.

<a name="phi"> </a>
### Protected Health Information (PHI) De-Identification 
Uzuner, Özlem, Yuan Luo, and Peter Szolovits. "Evaluating the state-of-the-art in automatic de-identification." Journal of the American Medical Informatics Association 14.5 (2007): 550-563.

Stubbs, Amber, and Özlem Uzuner. "Annotating longitudinal clinical narratives for de-identification: The 2014 i2b2/UTHealth corpus." Journal of biomedical informatics 58 (2015): S20-S29.

Stubbs, Amber, Michele Filannino, and Özlem Uzuner. "De-identification of psychiatric intake records: Overview of 2016 CEGS N-GRID Shared Tasks Track 1." Journal of biomedical informatics 75 (2017): S4-S18.

<a name="risk"> </a>
### Risk Factor Identification
Stubbs, Amber, et al. "Identifying risk factors for heart disease over time: Overview of 2014 i2b2/UTHealth shared task Track 2." Journal of biomedical informatics 58 (2015): S67-S77.

<a name="smoking"> </a>
### Smoking Status Classification 
Uzuner, Özlem, et al. "Identifying patient smoking status from medical discharge records." Journal of the American Medical Informatics Association 15.1 (2008): 14-24.

<a name="substance"> </a>
### Substance Abuse Detection 
Yetisgen, Meliha, and Lucy Vanderwende. "Automatic identification of substance abuse from social history in clinical text." Conference on Artificial Intelligence in Medicine in Europe. Springer, Cham, 2017.

<a name="symptoms"> </a>
### Symptom Severity Prediction
Filannino, Michele, Amber Stubbs, and Özlem Uzuner. "Symptom severity prediction from neuropsychiatric clinical records: Overview of 2016 CEGS N-GRID Shared Tasks Track 2." Journal of biomedical informatics 75 (2017): S62-S70.

<a name="general"> </a>
### General Non-Specific 
Uzuner, Özlem, Imre Solti, and Eithon Cadag. "Extracting medication information from clinical text." Journal of the American Medical Informatics Association 17.5 (2010): 514-518.

Uzuner, Özlem, et al. "2010 i2b2/VA challenge on concepts, assertions, and relations in clinical text." Journal of the American Medical Informatics Association 18.5 (2011): 552-556.

Uzuner, Ozlem, et al. "Evaluating the state of the art in coreference resolution for electronic medical records." Journal of the American Medical Informatics Association 19.5 (2012): 786-791.

Sun, Weiyi, Anna Rumshisky, and Ozlem Uzuner. "Evaluating temporal relations in clinical text: 2012 i2b2 Challenge." Journal of the American Medical Informatics Association 20.5 (2013): 806-813.

Suominen, Hanna, et al. "Overview of the ShARe/CLEF eHealth evaluation lab 2013." International Conference of the Cross-Language Evaluation Forum for European Languages. Springer, Berlin, Heidelberg, 2013.

Pradhan, Sameer, et al. "Semeval-2014 task 7: Analysis of clinical text." Proc. of the 8th International Workshop on Semantic Evaluation (SemEval 2014. 2014.
Goeuriot, Lorraine et al. “Overview of the CLEF eHealth Evaluation Lab 2015.” CLEF (2015).

Mowery, Danielle L., et al. "Normalizing acronyms and abbreviations to aid patient understanding of clinical texts: ShARe/CLEF eHealth Challenge 2013, Task 2." Journal of biomedical semantics 7.1 (2016): 1-13.

Savkov, Aleksandar, et al. "Annotating patient clinical records with syntactic chunks and named entities: the Harvey Corpus." Language resources and evaluation 50.3 (2016): 523-548.

Bethard, S., G. Savova, and M. Palmer. "SemEval-2017 Task 12." Clinical TempEval (2017).

Yue, Xiang, Bernal Jiménez Gutiérrez, and Huan Sun. "Clinical Reading Comprehension: A Thorough Analysis of the emrQA Dataset." Proceedings of the 58th Annual Meeting of the Association for Computational Linguistics. 2020.

Romanov, Alexey, and Chaitanya Shivade. "Lessons from Natural Language Inference in the Clinical Domain." Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing. 2018.

Henry, Sam, et al. "2018 n2c2 shared task on adverse drug events and medication extraction in electronic health records." Journal of the American Medical Informatics Association 27.1 (2020): 3-12.

Peng, Yifan, et al. "NegBio: a high-performance tool for negation and uncertainty detection in radiology reports." AMIA Summits on Translational Science Proceedings 2018 (2018): 188.

Wang, Yanshan, et al. "The 2019 n2c2/OHNLP track on clinical semantic textual similarity: overview." JMIR Medical Informatics 8.11 (2020): e23375.

Henry, Sam, et al. "The 2019 National Natural language processing (NLP) Clinical Challenges (n2c2)/Open Health NLP (OHNLP) shared task on clinical concept normalization for clinical records." Journal of the American Medical Informatics Association 27.10 (2020): 1529-1537.

Abacha, Asma Ben, Chaitanya Shivade, and Dina Demner-Fushman. "Overview of the mediqa 2019 shared task on textual inference, question entailment and question answering." Proceedings of the 18th BioNLP Workshop and Shared Task. 2019.

Viani, Natalia, et al. "Annotating Temporal Relations to Determine the Onset of Psychosis Symptoms." MedInfo. 2019.

Jagannatha, Abhyuday, et al. "Overview of the first natural language processing challenge for extracting medication, indication, and adverse drug events from electronic health record notes (MADE 1.0)." Drug safety 42.1 (2019): 99-111.

Wang, Ping, Tian Shi, and Chandan K. Reddy. "Text-to-sql generation for question answering on electronic medical records." Proceedings of The Web Conference 2020. 2020.

van Aken, Betty, et al. "Assertion Detection in Clinical Notes: Medical Language Models to the Rescue?." Proceedings of the Second Workshop on Natural Language Processing for Medical Conversations. 2021.

Abacha, Asma Ben, et al. "Overview of the mediqa 2021 shared task on summarization in the medical domain." Proceedings of the 20th Workshop on Biomedical Language Processing. 2021.

## Authors
This review is wrote by Dr.Yanjun Gao (ICU Data Science Lab, UW-Madison), Dr. Dmitriy Dligach (Loyola), Leslie Christensen, Sameuel Tesch, Ryan Laffin (UW-Madison),  Dr. Donfang Xu, Dr. Timothy Miller (Boston Children's Hospital, Harvard), Dr. Ozlem Uzuner (George Mason), Dr. Matthew M. Churpek and Dr. Majid Afshar (ICU Data Science Lab, UW-Madison). 

## Acknowledgement
Big appreciation to David Bloom (UW-Madison) and Anne Glorioso (UW-Madison) for their review of the search query as computer science librarians. 

## Contact 
For any question, please contact ygao@medicine.wisc.edu. 
