"""
Source code to generate search results on ACL Anthology BibTex
"""
from nltk.stem.snowball import *

stem = True 
write_results_to_csv = True 
# Download from acl anthology website 
path_to_acl_bib_file = "/Users/ygao/Desktop/systematic_reviews/anthology+abstracts_sep1.bib"
newlines = open(path_to_acl_bib_file).read().split("inproceedings")


if stem:
	stemmer = SnowballStemmer("english")


"""
Different search terms, developed by librarians 
Rule: 
look for the combination of search terms co-occuring in orig_search_term1, orig_search_term2, orig_search_term4
"""

orig_search_term1 = ["Electronic Health Records", "Patient Discharge" , "Patient Discharge Summaries" , "health record" , "medical record" , "patient discharge" , "discharge record" , "discharge summar" , "clinical narrative" , "clinical domain" , "medical domain" , "clinical note" , "clinical record" , "clinical text" , "medical text"] # Yanjun added medical text

orig_search_term2 = ["Natural Language Processing", "natural language","language processing", "translation", "NLP","inference", "generation" , "relation extraction", "relation-extraction", "entity recognition", "entity-recognition", "concept normalization","concept-normalisation","relation classification", "sequence label","summarization", "summarisation","parts-of-speech" ,"information-retrieval", "information retrieval", "informationgrouping", "sentiment", "semantic","temporal-expression","co-reference","word-sense-disambiguation",]

orig_search_term3 = ["similarity", "coherence", "feature"] 

orig_search_term4 = ["Datasets as Topic", "shared task", "shared-task", "annotat", "question-entailment", "question-answering", "challenge", "community", "bakeoff" "bake-off", "benchmark", "evaluation","assessment", "track", "public data", "public dataset", "public datasets", "publicly available dataset", "publicly available datasets", "public corpus"]

if stem:
	search_term1 = [stemmer.stem(i) for i in orig_search_term1]
	search_term2 = [stemmer.stem(i) for i in orig_search_term2]
	search_term4 = [stemmer.stem(i) for i in orig_search_term4]
else:
	search_term1 = orig_search_term1
	search_term2 = orig_search_term2
	search_term4 = orig_search_term4


def extractRecord(bibterm):
	biblines = bibterm.split("\n") 
	structured_items = {}
	for l in biblines[1:]:
		if "=" in l:
			item = l.split("=")
			structured_items[item[0].strip()] = item[1]
	return structured_items 

def SearchOverFullBib(newlines, flag=True):
	hits = []
	bibs = [] 
	urls = [] 
	for l in newlines:
		if stem:
			newl = stemmer.stem(l) 
		else:
			newl = l.lower()
		for j in search_term1:
			if j.lower() in newl:
				for k in search_term2:
					if (k.lower() in newl):
						for o in search_term4:
							if o.lower() in newl: 
								record = extractRecord(l) 
								if 'title' not in record:
									title = ''
								else:
									title = record['title']
								if 'abstract' not in record:
									abstract = ''
								else:
									abstract = record['abstract']
								hits.append([title,abstract, l, j, k, o])
								bibs.append(l)
								urls.append(record['url'])
								break
						break
				break

	print("Found {} Records from Full Bib Search".format(len(hits))) # 244 for search term 1, 2, 4
	return hits, bibs, urls   

def SearchOverTitle(newlines, flag=True):
	hits = []
	bibs = [] 
	urls = [] 
	for l in newlines:
		if len(l) == 0:
			continue 
		record = extractRecord(l) 
		if "title" not in record:
			#print(record) 
			continue 
		title = record['title']
		if stem:
			title = stemmer.stem(title)  
		for j in search_term1:
			if j.lower() in title:
				for k in search_term2:
					if (k.lower() in title):
						for o in search_term4:
							if o.lower() in title: 
								hits.append([title, j, k, o])
								bibs.append(l)
								urls.append(record['url'])
								break
						break
				break

	print("Found {} Records from Title Search".format(len(hits))) # 244 for search term 1, 2, 4
	return hits, bibs, urls   


def SearchOverAbstract(newlines, flag=True):
	hits = []
	bibs = [] 
	urls = [] 
	for l in newlines:
		if len(l) == 0:
			continue 
		record = extractRecord(l) 
		if "abstract" not in record:
			#print(record) 
			continue 
		abstract = record['abstract']
		if stem:
			abstract = stemmer.stem(abstract)
		for j in search_term1:
			if j.lower() in abstract:
				for k in search_term2:
					if (k.lower() in abstract):
						for o in search_term4:
							if o.lower() in abstract: 
								if 'title' in record:
									title = record['title']
									hits.append([title, abstract, j, k, o])
								else:
									hits.append(["", abstract, j,k,o])
								bibs.append(l)
								urls.append(record['url'])
								break
						break
				break

	print("Found {} Records from Abstract Search".format(len(hits))) # 244 for search term 1, 2, 4
	return hits, bibs, urls   


fulltext_search, fulltext_bibs, fulltext_urls = SearchOverFullBib(newlines,True)
title_search, title_bibs, title_urls = SearchOverTitle(newlines, True)
abstract_search, abstract_bibs, abstract_urls = SearchOverAbstract(newlines,True)

full_terms1 = []
full_terms2 = []
full_terms4 = [] 

# import urllib3
# def RetrieveEndNote(urls, search_mode):
# 	http = urllib3.PoolManager()

# 	with open(search_mode+"_stem"+str(stem)+"_endNotes.txt","w") as f:
# 		for link in urls:
# 			print("clean url: ",link.strip()[1:-2] )
# 			response = http.request('GET', link.strip()[1:-2]+".endf")
# 			f.write(response.data.decode('utf-8-sig')+"\n")

# RetrieveEndNote(title_urls, "TitleSearch")
# RetrieveEndNote(fulltext_urls, "FullSearch")
# RetrieveEndNote(abstract_urls, "AbstractSearch")

"""
Write searching results to csv file, make files based on fullbib/title/abstract searching 
"""

import csv 
output_to_csv_fullbib_search = "/Users/ygao/Desktop/systematic_reviews/acl_bib_search_fullbib_stem"+str(stem)+".csv"
output_to_csv_title_search = "/Users/ygao/Desktop/systematic_reviews/acl_bib_search_title_stem"+str(stem)+".csv" 
output_to_csv_abstract_search = "/Users/ygao/Desktop/systematic_reviews/acl_bib_search_abstract_stem"+str(stem)+".csv"  

with open(output_to_csv_fullbib_search,"w") as cout:
	wr = csv.writer(cout)
	wr.writerow(["Title", "Abstract", "Bib Item", "Matched Search Term 1", "Matched Search Term 2", "Matched Search Term 4"]) 
	for l in fulltext_search:
		wr.writerow(l) 
		full_terms1.append(l[3])
		full_terms2.append(l[4])
		full_terms4.append(l[5])


if write_results_to_csv:
	with open(output_to_csv_title_search,"w") as cout:
		wr = csv.writer(cout)
		wr.writerow(["Title", "Matched Search Term 1", "Matched Search Term 2", "Matched Search Term 4"]) 
		for l in title_search:
			wr.writerow(l) 

	with open(output_to_csv_abstract_search,"w") as cout:
		wr = csv.writer(cout)
		wr.writerow(["Tintle", "Abstract", "Matched Search Term 1", "Matched Search Term 2", "Matched Search Term 4"]) 
		for l in abstract_search:
			wr.writerow(l) 


from collections import Counter
term1_cnt = Counter(full_terms1)
term2_cnt = Counter(full_terms2)
term4_cnt = Counter(full_terms4)

if write_results_to_csv:
	with open("/Users/ygao/Desktop/systematic_reviews/acl_bib_search_term1_cnt_stem"+str(stem)+".csv","w") as cout:
		wr = csv.writer(cout)
		wr.writerow(["Term", "Cnt"]) 
		for k,v in term1_cnt.items():
			wr.writerow([k, v]) 

	with open("/Users/ygao/Desktop/systematic_reviews/acl_bib_search_term2_cnt_stem"+str(stem)+".csv","w") as cout:
		wr = csv.writer(cout)
		wr.writerow(["Term", "Cnt"]) 
		for k,v in term2_cnt.items():
			wr.writerow([k, v]) 


	with open("/Users/ygao/Desktop/systematic_reviews/acl_bib_search_term4_cnt_stem"+str(stem)+".csv","w") as cout:
		wr = csv.writer(cout)
		wr.writerow(["Term", "Cnt"]) 
		for k,v in term4_cnt.items():
			wr.writerow([k, v]) 





